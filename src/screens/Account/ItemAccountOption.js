import React from 'react';
import {View, StyleSheet, Image, Platform} from 'react-native';
import {Text} from 'react-native';
import {moderateScale} from '../../libs/scaling';
import Colors from '../../themes/Colors';
import {TouchableOpacity} from 'react-native-gesture-handler';

export default function ItemAccountOption(props) {
  const {item} = props;
  return (
    <>
      <TouchableOpacity style={styles.profileContainer}>
        <View
          style={
            Platform.OS === 'android'
              ? styles.border
              : [
                  styles.border,
                  {borderBottomWidth: 1, borderBottomColor: Colors.divider},
                ]
          }>
          <View style={styles.logoContainer}>
            <Image style={styles.logo} source={item.logo} />
          </View>

          <View style={styles.styleOption}>
            <Text>{item.profile}</Text>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image
                source={require('./../../assets/images/chevron-right.png')}
              />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </>
  );
}

const styles = StyleSheet.create({
  profileContainer: {
    height: null,
  },
  logoContainer: {
    width: moderateScale(30),
    height: moderateScale(30),
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: moderateScale(10),
    marginRight: moderateScale(10),
  },
  logo: {
    resizeMode: 'cover',
  },
  styleOption: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
    paddingRight: moderateScale(15),
  },
  border: {
    borderBottomColor: Colors.black,
    height: moderateScale(51),
    borderBottomWidth: 0.2,
    alignItems: 'center',
    textAlignVertical: 'center',
    flexDirection: 'row',
  },
});
