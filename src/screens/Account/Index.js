import {View, Text} from 'react-native';
import React, {useEffect, useState} from 'react';
import {Button, Pressable, TouchableOpacity} from 'react-native';
import FlatList from '../../components/FlatList';
import Colors from '../../themes/Colors';
import {moderateScale} from '../../libs/scaling';
import {StyleSheet} from 'react-native';
import Avatar from '../../components/Avatar';
import ItemAccountOption from './ItemAccountOption';
export default function Account() {
  const [dataProfile, setDataProfile] = useState([]);
  const [dataProfileOption, setDataProfileOption] = useState([]);
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    _loadProfile();
    _loadProfileOption();
  }, []);
  function _loadProfile() {
    const data1 = {
      id: 'ABC-12345',
      fullName: 'MoneyTracker account',
      photo: 'https://picsum.photos/200/300',
    };
    setDataProfile(data1);
  }
  function _loadProfileOption() {
    setLoading(true);
    const data2 = [
      {
        profile: 'Account',
        logo: require('../../assets/images/account-icon.png'),
      },
      {
        profile: 'Logout',
        logo: require('../../assets/images/account-icon.png'),
      },
    ];
    setDataProfileOption(data2);
    setLoading(false);
  }
  const _renderHeader = () => {
    return (
      <View>
        <View style={styles.containerTop}>
          <View style={styles.alignCenter}>
            <Avatar
              source={dataProfile?.photo ?? null}
              size={moderateScale(100)}
              style={styles.photo}
            />
            <Text style={{fontSize: moderateScale(16), fontWeight: 'bold'}}>
              TEST
            </Text>
            <Text style={{fontSize: moderateScale(16)}}>12345</Text>
          </View>
        </View>
      </View>
    );
  };
  const _renderItemProfile = ({item}) => {
    return <ItemAccountOption item={item} />;
  };
  return (
    <View style={styles.container}>
      <FlatList
        data={dataProfileOption}
        ListHeaderComponent={_renderHeader}
        renderItem={_renderItemProfile}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bgBackground,
  },
  containerTop: {
    flex: 1,
    flexDirection: 'row',
    minHeight: moderateScale(210),
    maxHeight: 'auto',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    top: 0,
  },
  alignCenter: {
    minHeight: moderateScale(200),
    alignItems: 'center',
    justifyContent: 'center',
  },
  photo: {
    marginBottom: moderateScale(10),
    marginTop: moderateScale(16),
  },
  textProfile: {
    fontSize: moderateScale(12),
    paddingLeft: moderateScale(12),
    marginTop: moderateScale(3),
    backgroundColor: Colors.greyBackground,
  },
  styleTitle: {
    fontSize: 20,
    fontWeight: '600',
  },
});
