import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';

import FlatList from '../../components/FlatList';
import ScrollView from '../../components/ScrollView';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Text} from '../../components/Text';

import Colors from '../../themes/Colors';
import {moderateScale} from '../../libs/scaling';

import ItemCategory from './ItemCategory';

import {setCategories, setCategory} from '../../redux/actions';
import {useSelector, useDispatch} from 'react-redux';

export default function Category(props) {
  const [dataSource, setDataSource] = useState([]);
  const [loading, setLoading] = useState(false);
  const DEFAULT_LOGO = require('../../assets/images/account-icon.png');

  useEffect(() => {
    _loadDataSource();
  }, []);

  const dispatch = useDispatch();

  useEffect(() => {
    props.navigation.setOptions({
      headerShown: true,
      headerLeft: () => {
        return (
          <TouchableOpacity
            onPress={() => {
              _toTransaction();
            }}
            style={{marginLeft: moderateScale(12)}}>
            <Image
              source={require('../../assets/images/arrow_left.png')}></Image>
          </TouchableOpacity>
        );
      },
    });
  }, [props.navigation]);

  useEffect(() => {
    props.navigation.setOptions({
      headerShown: true,
      headerRight: () => {
        return (
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate('Add New Category');
            }}
            style={{marginLeft: moderateScale(12)}}>
            <Text
              style={{
                marginRight: 18,
                marginLeft: 10,
                marginTop: 4,
                fontSize: moderateScale(15),
                fontWeight: 'bold',
                color: Colors.textDark,
              }}>
              Add
            </Text>
          </TouchableOpacity>
        );
      },
    });
  }, [props.navigation]);

  function _toTransaction() {
    props.navigation.navigate('Add Transaction', {title: '', logo: ''});
  }

  function _toAddTransaction(titles, logos) {
    props.navigation.navigate({
      name: 'Add Transaction',
      params: {title: titles, logo: logos},
      merge: true,
    });
  }

  function _loadDataSource() {
    setLoading(true);
    const map =
      categories.length > 0
        ? categories.map(item => {
            return item.categoryName;
          })
        : '';
    const test = [
      {
        id: 1,
        title: 'Food Beverage',
        logo: 'food',
      },
      {
        id: 2,
        title: 'Transportation',
        logo: 'car',
      },
      {
        id: 3,
        title: 'Salary',
        logo: 'wallet',
      },
    ];
    setDataSource(test);
    setTimeout(() => {
      setLoading(false);
    }, 1000);
  }

  const _renderItemCategory = ({item}) => {
    return (
      <ItemCategory
        item={item}
        onPress={() => {
          _toAddTransaction(item.title, item.logo);
        }}
      />
    );
  };

  const {categories} = useSelector(state => state.wallet);

  return (
    <View style={styles.container}>
      <ScrollView>
        {categories.length > 0 ? (
          categories.map(item => (
            <TouchableOpacity
              style={styles.profilesContainer}
              onPress={() => {
                _toAddTransaction(item.categoryName, item.categoryIcon);
              }}>
              <View
                style={
                  Platform.OS === 'android'
                    ? styles.border
                    : [
                        styles.border,
                        {
                          borderBottomWidth: 1,
                          borderBottomColor: Colors.divider,
                        },
                      ]
                }>
                <View style={styles.logoContainer}>
                  <Icon
                    size={24}
                    color={Colors.primary}
                    name={item.categoryIcon}
                  />
                </View>

                <View style={styles.styleOption}>
                  <Text>{item.categoryName}</Text>
                </View>
              </View>
            </TouchableOpacity>
          ))
        ) : (
          <Text>No New Category Added</Text>
        )}
        <FlatList data={dataSource} renderItem={_renderItemCategory} />
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bgBackground,
  },
  profileContainer: {
    height: null,
  },
  logoContainer: {
    width: moderateScale(30),
    height: moderateScale(30),
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: moderateScale(10),
    marginRight: moderateScale(10),
  },
  logo: {
    resizeMode: 'cover',
  },
  styleOption: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
    paddingRight: moderateScale(15),
  },
  border: {
    borderBottomColor: Colors.black,
    height: moderateScale(51),
    borderBottomWidth: 0.2,
    alignItems: 'center',
    textAlignVertical: 'center',
    flexDirection: 'row',
  },
  profilesContainer: {
    //top: moderateScale(-390),
  },
});
