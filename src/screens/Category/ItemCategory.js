import React from 'react';
import {View, StyleSheet, Image, Platform} from 'react-native';
import {Text} from '../../components/Text';
import {moderateScale} from '../../libs/scaling';
import Colors from '../../themes/Colors';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default function ItemCategory(props) {
  const {item, onPress} = props;
  return (
    <>
      <TouchableOpacity
        style={styles.profileContainer}
        onPress={() => onPress()}>
        <View
          style={
            Platform.OS === 'android'
              ? styles.border
              : [
                  styles.border,
                  {borderBottomWidth: 1, borderBottomColor: Colors.divider},
                ]
          }>
          <View style={styles.logoContainer}>
            <Icon size={24} color={Colors.primary} name={item.logo} />
          </View>

          <View style={styles.styleOption}>
            <Text>{item.title}</Text>
          </View>
        </View>
      </TouchableOpacity>
    </>
  );
}

const styles = StyleSheet.create({
  profileContainer: {
    // top: moderateScale(50),
    height: null,
  },
  logoContainer: {
    width: moderateScale(30),
    height: moderateScale(30),
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: moderateScale(10),
    marginRight: moderateScale(10),
  },
  logo: {
    resizeMode: 'cover',
  },
  styleOption: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
    paddingRight: moderateScale(15),
  },
  border: {
    borderBottomColor: Colors.black,
    height: moderateScale(51),
    borderBottomWidth: 0.2,
    alignItems: 'center',
    textAlignVertical: 'center',
    flexDirection: 'row',
  },
});
