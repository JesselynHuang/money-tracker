import React, {useState, useEffect} from 'react';

import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  Platform,
} from 'react-native';
import {moderateScale} from '../../libs/scaling';
import Colors from '../../themes/Colors';

import {Picker} from '@react-native-picker/picker';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {PrimaryButton} from '../../components/Button';
import {useDispatch, useSelector} from 'react-redux';
import {setCategories} from '../../redux/actions';

export default function AddNewCategory(props) {
  const {route, navigation} = props;

  const dispatch = useDispatch();

  const [selectType, setSelectedType] = useState('income');
  const [categoryName, setCategoryName] = useState('');
  const [limit, setLimit] = useState('');
  const [categoryIcon, setCategoryIcon] = useState('');

  function _createCategory() {
    let payload = {
      categoryName: categoryName,
      limit: limit,
      categoryIcon: route.params?.icon === '' ? 'wallet' : route.params.icon,
    };

    console.log('payload-->', payload);

    dispatch(setCategories(payload));
    navigation.navigate('Choose Category');
  }

  const handleInputCategoryName = value => {
    // console.log('value category name--->', value);
    setCategoryName(value);
  };

  const handleInputLimit = value => {
    // console.log('value limit--->', value);
    setLimit(value);
  };

  const handleInputCategoryIcon = value => {
    console.log('value category icon --->', categoryIcon);
    setCategoryIcon(value);
  };
  const {categories} = useSelector(state => state.wallet);
  useEffect(() => {
    navigation.setOptions({
      headerShown: true,
      headerLeft: () => {
        return (
          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Add Transaction');
            }}
            style={{marginLeft: moderateScale(12)}}>
            <Image
              source={require('../../assets/images/arrow_left.png')}></Image>
          </TouchableOpacity>
        );
      },
    });
  }, [navigation]);

  useEffect(() => {
    if (route.params?.name && route.params?.icon) {
    }
  }, [route.params]);

  return (
    <View style={styles.container}>
      <View
        style={
          Platform.OS === 'android'
            ? styles.border
            : [
                styles.border,
                {borderBottomWidth: 1, borderBottomColor: Colors.divider},
              ]
        }>
        <View style={styles.logoContainer}>
          <Icon size={24} color={Colors.primary} name="chevron-triple-right" />
        </View>
        <View>
          <View style={styles.styleOption}>
            <TextInput
              style={styles.textInput}
              value={categoryName}
              placeholder="Category Name"
              onChangeText={handleInputCategoryName}
            />
          </View>
        </View>
      </View>
      <View style={[styles.border, {justifyContent: 'space-between'}]}>
        <Text
          style={
            ([styles.label],
            {
              left: moderateScale(10),
              alignItems: 'center',
            })
          }>
          Income Expense
        </Text>
        <Picker
          selectedValue={selectType}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => setSelectedType(itemValue)}>
          <Picker.item label="Income" value="income" />
          <Picker.item label="Expense" value="expense" />
        </Picker>
      </View>
      {selectType === 'income' ? null : (
        <View style={[styles.border, {justifyContent: 'space-between'}]}>
          <Text
            style={
              ([styles.label],
              {
                left: moderateScale(10),
                alignItems: 'center',
              })
            }>
            Spending Limit
          </Text>
          <TextInput
            style={{right: moderateScale(10)}}
            keyboardType="numeric"
            placeholder="Enter Limit"
            value={limit}
            onChangeText={handleInputLimit}></TextInput>
        </View>
      )}
      <View
        style={
          Platform.OS === 'android'
            ? styles.border
            : [
                styles.border,
                {borderBottomWidth: 1, borderBottomColor: Colors.divider},
              ]
        }>
        <View style={styles.logoContainer}>
          <Icon
            size={24}
            color={Colors.primary}
            name={
              route.params?.icon === undefined ? 'wallet' : route.params?.icon
            }
          />
        </View>

        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Add Icon');
          }}
          style={styles.styleOption}>
          <Text>
            {route.params?.name === undefined ? 'cash' : route.params?.name}
          </Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image
              source={require('./../../assets/images/chevron-right.png')}
            />
          </View>
        </TouchableOpacity>
      </View>
      <PrimaryButton
        style={styles.createButton}
        title="Create"
        onPress={() => {
          _createCategory();
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingTop: moderateScale(20),
    backgroundColor: Colors.bgBackground,
    minHeight: moderateScale(50),
    height: '100%',
    width: '100%',
  },
  label: {
    marginLeft: 15,
    fontSize: moderateScale(16),
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textInput: {
    width: 260,
    fontSize: moderateScale(16),
    height: 50,
    marginLeft: 15,
    marginTop: moderateScale(2),
    marginBottom: moderateScale(-5),
  },
  picker: {
    fontSize: moderateScale(16),
    height: 50,
    width: 150,
  },
  logoContainer: {
    width: moderateScale(30),
    height: moderateScale(30),
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: moderateScale(10),
    marginRight: moderateScale(10),
  },
  logo: {
    resizeMode: 'cover',
  },
  styleOption: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
    paddingRight: moderateScale(15),
  },
  border: {
    borderBottomColor: Colors.black,
    height: moderateScale(51),
    borderBottomWidth: 0.2,
    alignItems: 'center',
    textAlignVertical: 'center',
    flexDirection: 'row',
  },
  createButton: {
    marginLeft: 80,
    marginTop: moderateScale(50),
    paddingTop: 5,
    borderRadius: 10,
    width: 200,
    height: 40,
    fontSize: moderateScale(16),
    backgroundColor: Colors.primary,
  },
});
