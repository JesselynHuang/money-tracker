import React, {useEffect} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Alert} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useDispatch, useSelector} from 'react-redux';

import ScrollView from '../../components/ScrollView';
import {Card} from '../../components/Card';

import {moderateScale} from '../../libs/scaling';
import Colors from '../../themes/Colors';

export default function Transaction(props) {
  const transactions = useSelector(state => state.wallet.transactions);
  const dispatch = useDispatch();
  useEffect(() => {
    props.navigation.setOptions({
      headerShown: true,
      headerRight: () => {
        return (
          <TouchableOpacity
            style={{right: moderateScale(15)}}
            onPress={() => {
              _toAddTransaction();
            }}>
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  fontSize: moderateScale(15),
                }}>
                Add
              </Text>
              <Icon style={{marginLeft: moderateScale(5)}} name="plus"></Icon>
            </View>
          </TouchableOpacity>
        );
      },
    });
    console.log('tras screen-->', transactions);
  }, [props.navigation]);

  function _toAddTransaction() {
    props.navigation.navigate('Add Transaction', {title: '', logo: ''});
  }

  // const {dateTransaction, total, category, wallet, description} = useSelector(
  //   state => state.walletReducer,
  // );
  // const dispatch = useDispatch();

  return (
    <View style={styles.container}>
      <ScrollView>
        {transactions.map((item, i) => {
          return (
            <React.Fragment key={i}>
              <View style={styles.ViewTitle}>
                <Text style={styles.walletTitle}>{item.walletName}</Text>
                <Text>{item.date}</Text>
              </View>
              <Card style={styles.containerCard}>
                <View style={styles.bodyCard}>
                  <Text style={styles.date}>{item.desc}</Text>
                  <Text style={styles.total}>{item.total}</Text>
                  <View style={{display: 'flex', flexDirection: 'row'}}>
                    <Icon
                      style={{marginRight: moderateScale(5)}}
                      size={25}
                      color={Colors.primary}
                      name={item.categoryLogos}></Icon>
                    <Text style={styles.category}>{item.category}</Text>
                  </View>
                </View>
              </Card>
            </React.Fragment>
          );
        })}
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.bgBackground,
    height: '100%',
  },
  containerCard: {
    flex: 1,
    //width: 200,
    height: 100,
    //marginLeft: 25,
    marginTop: 8,
    //flexDirection: 'row',
    minWidth: moderateScale(300),
    minHeight: moderateScale(80),
    maxHeight: moderateScale(140),
    // //borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ViewTitle: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: moderateScale(5),
  },
  bodyCard: {
    minWidth: '75%',
    maxWidth: '85%',
  },
  total: {
    textAlign: 'right',
    fontSize: moderateScale(24),
    color: Colors.darkBlue,
  },
  walletTitle: {
    marginLeft: 15,
    fontSize: moderateScale(16),
    fontWeight: 'bold',
  },
  category: {
    color: Colors.textDark,
    fontSize: moderateScale(14),
  },
  date: {},
  description: {
    color: Colors.textDark,
    fontSize: moderateScale(14),
  },
});
