import React, {useState, useLayoutEffect, useEffect} from 'react';
import {addTransaction, setCategory, setWallet} from '../../redux/actions';
import {useSelector, useDispatch} from 'react-redux';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {v4 as uuidv4} from 'uuid';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
  Button,
} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {PrimaryButton} from '../../components/Button';
import NumberFormat from 'react-number-format';
import {moderateScale} from '../../libs/scaling';
import moment from 'moment';
import Colors from '../../themes/Colors';
import {ReactNativeNumberFormat} from '../../components/NumberFormat';
export default function AddTransaction(props) {
  const {route, navigation} = props;
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [dates, setDates] = useState('');
  const [totalTransaction, setTotalTransaction] = useState(0);
  const [selectedValueWallet, setSelectedValueWallet] = useState('');
  const [description, setDescription] = useState('');
  const [open, setOpen] = useState(false);
  const wallet = useSelector(state => state.wallet);
  const map =
    wallet.wallets.length > 0
      ? wallet.wallets.map(item => {
          return item.name;
        })
      : '';

  const [defaultPicker, setDefaultPicker] = useState(
    map === '' ? 'No wallet choose' : map[0],
  );
  const trans = useSelector(state => state.wallet.transactions);
  const dispatch = useDispatch();
  useEffect(() => {
    if (route.params?.title && route.params?.logo) {
    }
    console.log(trans);
  }, [route.params]);
  useEffect(() => {
    props.navigation.setOptions({
      headerShown: true,
      headerLeft: () => {
        return (
          <TouchableOpacity
            onPress={() => {
              _toTransaction();
            }}
            style={{marginLeft: moderateScale(12)}}>
            <Image
              source={require('../../assets/images/arrow_left.png')}></Image>
          </TouchableOpacity>
        );
      },
    });
    console.log('date transaction-->', dates);
    console.log('route', props.route.params);
  }, [props.navigation, selectedValueWallet, wallet, dates]);
  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = date => {
    setDates(moment(date).format('DD MMM YYYY'));
    hideDatePicker();
  };

  function _pickDate(item) {
    setDateTransaction(item);
  }

  function _toTransaction() {
    props.navigation.navigate('Transactions');
  }

  function _toTransactionSubmit() {
    let payload = {
      id: uuidv4(),
      total: totalTransaction === '' ? '0' : totalTransaction,
      walletName: defaultPicker,
      date: dates === '' ? moment(Date.now()).format('DD MMM YYYY') : dates,
      category:
        route.params?.title === '' ? 'No Category ' : route.params?.title,
      categoryLogos: route.params?.logo === '' ? 'wallet' : route.params?.logo,
      desc: description === '' ? 'No description' : description,
    };
    dispatch(addTransaction(payload));
    props.navigation.navigate('Transactions');
  }

  function _toAddCategory() {
    props.navigation.navigate('Choose Category');
  }

  function _regexTransaction(e) {
    setTotalTransaction(e.replace(/[^0-9]/g, ''));
  }
  const category = useSelector(state => state.wallet.category);
  return (
    <View style={styles.container}>
      <View style={styles.containerChild}>
        <View style={styles.containerTransaction}>
          <View style={styles.flexRow}>
            <Text style={styles.label}>RP</Text>
            <NumberFormat
              value={totalTransaction}
              displayType={'text'}
              thousandSeparator={true}
              renderText={value => (
                <TextInput
                  underlineColorAndroid="transparent"
                  style={styles.textInput}
                  onChangeText={value => _regexTransaction(value)}
                  value={value}
                  keyboardType="numeric"
                />
              )}
            />
          </View>
          <TouchableOpacity style={styles.flexRow} onPress={showDatePicker}>
            <Icon
              size={20}
              color={Colors.primary}
              style={styles.label}
              name="clipboard-list"></Icon>

            <Text
              style={styles.textInput}
              placeholder="Pick Date"
              onChangeText={value => {
                setTotalTransaction(value);
              }}>
              {dates === '' ? 'Today' : dates}
            </Text>
          </TouchableOpacity>
          <DateTimePickerModal
            isVisible={isDatePickerVisible}
            mode="datetime"
            onConfirm={handleConfirm}
            onCancel={hideDatePicker}
          />

          <TouchableOpacity style={styles.flexRow}>
            <Icon
              style={styles.label}
              color={Colors.primary}
              size={20}
              name={
                route.params?.logo === ''
                  ? 'chevron-triple-right'
                  : route.params?.logo
              }></Icon>
            <Text style={styles.textInput} onPress={() => _toAddCategory()}>
              {route.params?.title === ''
                ? 'Select Category'
                : route.params?.title}
            </Text>
          </TouchableOpacity>

          <View style={styles.flexRow}>
            <Icon
              style={styles.label}
              color={Colors.primary}
              size={20}
              name="wallet"></Icon>
            {wallet.wallets.length > 0 ? (
              <Picker
                selectedValue={defaultPicker}
                style={styles.picker}
                onValueChange={(itemValue, itemIndex) =>
                  setDefaultPicker(itemValue)
                }>
                {wallet.wallets.map((item, i) => {
                  return (
                    <Picker.Item
                      key={i}
                      value={item.name}
                      label={item.name}></Picker.Item>
                  );
                })}
              </Picker>
            ) : (
              <TouchableOpacity
                onPress={() => props.navigation.navigate('Add Wallet')}>
                <Text style={styles.textInput}>Insert Wallet First</Text>
              </TouchableOpacity>
            )}
          </View>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <TextInput
              multiline
              numberOfLines={4}
              style={styles.textArea}
              onChangeText={value => setDescription(value)}
            />
          </View>

          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: moderateScale(100),
            }}>
            <PrimaryButton
              style={styles.createButton}
              title="Create Transaction"
              onPress={() => _toTransactionSubmit()}
            />
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.bgBackground,
    minHeight: moderateScale(50),
    height: '100%',
    width: '100%',
    paddingTop: 30,
  },
  containerChild: {
    backgroundColor: Colors.white,
    height: null,
    width: '100%',
    alignItems: 'center',
  },
  containerTransaction: {
    width: '90%',
  },
  flexRow: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  label: {
    width: 30,
    height: moderateScale(50),
    textAlignVertical: 'center',
  },
  buttonDate: {
    width: 150,
    height: 30,
    borderRadius: 10,
    backgroundColor: Colors.grayShadow,
  },
  textInput: {
    width: '100%',
    fontSize: moderateScale(16),
    borderBottomWidth: 0.3,
    height: moderateScale(50),
    textAlignVertical: 'center',
  },
  categoryText: {
    fontSize: moderateScale(16),
    fontWeight: 'bold',
  },
  picker: {
    fontSize: moderateScale(16),
    height: 50,
    minWidth: moderateScale(200),
    maxHeight: moderateScale(350),
  },
  textArea: {
    marginTop: 5,
    marginLeft: 15,
    width: 300,
    height: 100,
    borderWidth: 0.5,
    textAlignVertical: 'top',
  },
  createButton: {
    paddingTop: 5,
    borderRadius: 10,
    width: 200,
    height: 40,
    fontSize: moderateScale(16),
    backgroundColor: Colors.primary,
  },
  textAndLogo: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  plus: {},
});
