import React, {useEffect, useState, useLayoutEffect} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';

import ScrollView from '../../components/ScrollView';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import Colors from '../../themes/Colors';
import metrics from '../../libs/Metrics';
import {moderateScale} from '../../libs/scaling';

export default function AddIcon(props) {
  const [loading, setLoading] = useState(false);
  const [dataIcon, setDataIcon] = useState([]);

  useEffect(() => {
    props.navigation.setOptions({
      headerShown: true,
      headerLeft: () => {
        return (
          <TouchableOpacity
            onPress={() => {
              props.navigation.navigate('Add New Category');
            }}
            style={{marginLeft: moderateScale(12)}}>
            <Image
              source={require('../../assets/images/arrow_left.png')}></Image>
          </TouchableOpacity>
        );
      },
    });
  }, [props.navigation]);

  useEffect(() => {
    _loadData();
  }, []);

  function _loadData() {
    setLoading(true);
    const data = [
      {name: 'home', icon: 'home'},
      {name: 'tshirt-crew', icon: 'tshirt-crew'},
      {name: 'water', icon: 'water'},
      {name: 'phone', icon: 'phone'},
      {name: 'book', icon: 'book'},
      {name: 'baseball', icon: 'baseball'},
    ];
    setDataIcon(data);
  }

  function setToNewIcon(name, icon) {
    props.navigation.navigate({
      name: 'Add New Category',
      params: {
        name: name,
        icon: icon,
      },

      merge: true,
    });
  }
  return (
    <ScrollView
      contentContainerStyle={{justifyContent: 'center', alignItems: 'center'}}>
      <View style={styles.mainContainer}>
        <View style={styles.container}>
          {dataIcon.map(item => {
            return (
              <>
                <TouchableOpacity
                  style={styles.logoContainer}
                  onPress={() => {
                    console.log(setToNewIcon);
                    setToNewIcon(item.name, item.icon);
                  }}>
                  <Icon
                    color={Colors.primary}
                    size={35}
                    name={item.name}></Icon>
                  {props.route.params?.name}
                </TouchableOpacity>
              </>
            );
          })}
        </View>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    //width: null,
    height: metrics.screenHeight * 0.9,
    backgroundColor: Colors.white,
    borderColor: '#000',
    width: metrics.screenWidth * 1,
    top: 10,
  },
  container: {
    flexDirection: 'row',
    widht: moderateScale(50),
    flexWrap: 'wrap',
  },
  icon: {
    minWidht: moderateScale(50),
    height: moderateScale(20),
    borderWidth: 1,
    borderColor: '#12345',
  },
  logoContainer: {
    width: moderateScale(40),
    height: moderateScale(50),
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: moderateScale(10),
    marginRight: moderateScale(10),
  },
});
