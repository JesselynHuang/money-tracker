import {
  View,
  TouchableOpacity,
  StyleSheet,
  Text,
  // ScrollView,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {useIsFocused} from '@react-navigation/native';

import {moderateScale} from '../../libs/scaling';
import {Card} from '../../components/Card';
import ScrollView from '../../components/ScrollView';

import Colors from '../../themes/Colors';
import Icon from 'react-native-vector-icons/AntDesign';

const Wallet = props => {
  const {navigation} = props;

  const {wallets} = useSelector(state => state.wallet);

  // const isFocused = useIsFocused();

  // useEffect(() => {
  //   if (isFocused) {
  //     if (wallets.length === 0) {
  //       _toAddWallet();
  //     }
  //   }
  // }, [wallets]);

  function _toAddWallet() {
    navigation.navigate('Add Wallet');
  }

  useEffect(() => {
    navigation.setOptions({
      headerShown: true,
      headerRight: () => {
        return (
          <TouchableOpacity
            style={{
              right: moderateScale(15),
            }}
            onPress={() => {
              navigation.navigate('Add Wallet');
            }}>
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{fontSize: moderateScale(15)}}>Add</Text>
              <Icon style={{marginLeft: moderateScale(5)}} name="plus"></Icon>
            </View>
          </TouchableOpacity>
        );
      },
    });
  }, [navigation]);

  return (
    <View style={styles.container}>
      <ScrollView>
        {wallets.length > 0 ? (
          wallets.map(item => (
            <Card style={styles.container1}>
              <Icon style={styles.iconWallet} name="wallet"></Icon>
              <View style={styles.bodyText}>
                <Text style={styles.title}>{item.name}</Text>
                <Text
                  style={{
                    marginTop: moderateScale(-5),
                    marginLeft: moderateScale(130),
                    fontSize: moderateScale(16),
                    color: Colors.darkBlue,
                    fontWeight: 'bold',
                  }}>
                  {item.currency} {item.balance}
                </Text>
              </View>
            </Card>
          ))
        ) : (
          <Text>No Wallet Found</Text>
        )}
      </ScrollView>
    </View>
  );
};

export default Wallet;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Colors.bgBackground,
    paddingHorizontal: moderateScale(16),
  },
  container1: {
    flex: 1,
    height: 70,
    marginTop: 10,
    flexDirection: 'row',
    minWidth: moderateScale(300),
    minHeight: moderateScale(40),
    maxHeight: moderateScale(80),
    overflow: 'scroll',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: {
    width: moderateScale(36),
    height: moderateScale(36),
    backgroundColor: Colors.darkBlue,
    alignItems: 'center',
    borderRadius: 3,
    marginRight: moderateScale(9),
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  bodyText: {
    minWidth: '75%',
    maxWidth: '85%',
  },
  title: {
    marginTop: 22,
    fontSize: moderateScale(18),
    fontWeight: 'bold',
    fontFamily: 'poppins',
    marginLeft: 12,
  },
  iconWallet: {
    fontSize: 36,
    marginTop: 5,
    marginLeft: -25,
    color: Colors.primary,
  },
});
