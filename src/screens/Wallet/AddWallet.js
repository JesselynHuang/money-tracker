import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import {v4 as uuidv4} from 'uuid';

import {setWallet} from '../../redux/actions';

import {
  Text,
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Image,
} from 'react-native';

import {Picker} from '@react-native-picker/picker';
import {PrimaryButton} from '../../components/Button';

import {moderateScale} from '../../libs/scaling';
import Colors from '../../themes/Colors';

export default function AddWallet(props) {
  const [name, setName] = useState('');
  const [currency, setCurrency] = useState('IDR');
  const [balance, setBalance] = useState('');
  // const [dataWallet, setDataWallet] = useState([]);
  const {wallets = []} = props;
  useEffect(() => {
    props.navigation.setOptions({
      headerShown: true,

      headerLeft: () => {
        return (
          <TouchableOpacity
            onPress={() => props.navigation.navigate('Wallets')}
            style={{marginLeft: moderateScale(12)}}>
            <Image
              source={require('../../assets/images/arrow_left.png')}></Image>
          </TouchableOpacity>
        );
      },
    });
  }, [props.navigation]);

  function _toWallet() {
    let payload = {
      id: uuidv4(),
      name: name,
      currency: currency,
      balance: balance,
    };

    dispatch(setWallet(payload));

    props.navigation.navigate('Wallets');
  }

  const handleInputName = value => {
    setName(value);
  };

  const handleInputCurrency = itemValue => {
    setCurrency(itemValue);
  };

  const handleInputBalance = value => {
    setBalance(value);
  };

  const dispatch = useDispatch();
  return (
    <View style={styles.container}>
      <Text style={styles.header}>Money Tracker</Text>
      <Text style={styles.content}>
        Money Tracker help you to keep track your money from your Wallet Each
        wallet represents a source of money such as Cash or a Bank Account
      </Text>

      <TextInput
        style={styles.textInputName}
        value={name}
        placeholder="Wallet Name"
        onChangeText={handleInputName}
      />

      <Picker
        selectedValue={currency}
        style={styles.picker}
        onValueChange={(itemValue, itemIndex) =>
          handleInputCurrency(itemValue)
        }>
        <Picker.Item label="IDR" value="idr" />
        <Picker.Item label="SGD" value="sgd" />
      </Picker>

      <TextInput
        style={styles.textInputBalance}
        value={balance}
        keyboardType="numeric"
        placeholder="Starting Balance"
        onChangeText={handleInputBalance}
      />

      <PrimaryButton
        style={styles.primaryButton}
        title="Create"
        onPress={_toWallet}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: -38,
    backgroundColor: Colors.bgBackground,
    minHeight: moderateScale(50),
    height: '120%',
    width: '100%',
  },
  picker: {
    //fontSize: moderateScale(20),
    marginTop: moderateScale(-48),
    height: 50,
    width: 120,
  },
  content: {
    paddingTop: 5,
    marginTop: 10,
    marginLeft: 8,
    textAlign: 'center',
    fontSize: moderateScale(14),
  },
  header: {
    fontSize: 32,
    color: Colors.primary,
    fontWeight: 'bold',
    textAlign: 'center',
    fontColor: '#000000',
    marginTop: moderateScale(60),
  },
  textInputName: {
    width: 210,
    fontSize: moderateScale(16),
    borderBottomWidth: 1,
    height: 50,
    marginLeft: moderateScale(120),
    lineHeight: moderateScale(25),
    marginTop: moderateScale(70),
    //marginBottom: moderateScale(-10),
  },
  textInputBalance: {
    width: 210,
    fontSize: moderateScale(16),
    borderBottomWidth: 1,
    height: 50,
    marginLeft: moderateScale(120),
    //lineHeight: moderateScale(25),
    marginTop: moderateScale(10),
    //marginBottom: moderateScale(-10),
  },
  primaryButton: {
    paddingTop: 5,
    marginTop: moderateScale(120),
    marginLeft: moderateScale(80),
    borderRadius: 10,
    width: 200,
    height: 40,
    backgroundColor: Colors.primary,
  },
});
