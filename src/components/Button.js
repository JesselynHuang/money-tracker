import React from 'react';
import {Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {moderateScale} from '../libs/scaling';
import Colors from '../themes/Colors';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderRadius: moderateScale(4),
    paddingVertical: moderateScale(8),
    paddingHorizontal: moderateScale(18),
    justifyContent: 'center',
    alignItems: 'center',
  },
  PrimaryButtonBackground: {
    backgroundColor: Colors.primary,
  },
  disabledButtonBackground: {
    backgroundColor: Colors.greyButton,
  },
  txtStyle: {
    fontSize: moderateScale(18),
    textAlign: 'center',
    color: Colors.white,
  },
});

function PrimaryButton(props) {
  const {title, onPress, disabled, style, textStyle} = props;
  const buttonBg = disabled ? styles.disabledButtonBackground : Colors.primary;
  return (
    <TouchableOpacity
      style={style}
      disabled={disabled}
      onPress={() => onPress()}>
      <Text style={[styles.txtStyle, textStyle]}>{title}</Text>
    </TouchableOpacity>
  );
}
export {PrimaryButton};
