import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Colors from '../themes/Colors';

import Wallet from '../screens/Wallet/Wallet';
import Report from '../screens/Report/Report';
import Transaction from '../screens/Transaction/Transaction';
import AddTransaction from '../screens/Transaction/AddTransaction';
import Account from '../screens/Account/Index';
// import Add from './screens/Add';

import {moderateScale} from '../libs/scaling';
//import {PrimaryButton} from './Button';
import IconWithBadge from './IconWithBadge';

// const Adds = 'Add';
const Tab = createBottomTabNavigator();

const CustomTabBarButton = ({children, onPress}) => (
  <TouchableOpacity
    style={{
      top: -30,
      justifyContent: 'center',
      alignItems: 'center',
      ...styles.shadow,
    }}
    onPress={onPress}>
    <View
      style={{
        width: 70,
        height: 70,
        borderRadius: 35,
        backgroundColor: Colors.primary,
      }}>
      {children}
    </View>
  </TouchableOpacity>
);

export default function Dashboard(props) {
  return (
    <>
      <Tab.Navigator
        screenOptions={({route}) => ({
          headerShown: true,
          tabBarStyle: {
            position: 'relative',
            bottom: 2,
            left: 0,
            right: 20,
            elevation: 0,
            backgroundColor: '#fff',
            borderRadius: 15,
            height: 70,
            ...styles.shadow,
          },
          tabBarIcon: ({focused, color, size}) => {
            let badgeTotal = 0;
            if (route.name === 'Notification') {
              badgeTotal = props.badge;
            }
            return (
              <IconWithBadge
                name={route.name}
                color={color}
                focused={focused}
                badgeCount={badgeTotal}
              />
            );
          },
        })}
        tabBarOptions={{
          activeTintColor: Colors.primary,
          inactiveTintColor: 'gray',
          safeAreaInset: {bottom: 'never', top: 'never'},
        }}>
        <Tab.Screen
          name={'Wallets'}
          component={Wallet}
          options={{tabBarLabel: 'Wallet'}}
          options={{tabBarLabelStyle: {top: -10}}}></Tab.Screen>
        <Tab.Screen
          name={'Reports'}
          component={Report}
          options={{tabBarLabelStyle: {top: -10}}}></Tab.Screen>
        {/* <Tab.Screen
          name={'Add Transaction'}
          component={AddTransaction}
          options={{tabBarLabel: ''}}></Tab.Screen> */}
        <Tab.Screen
          name={'Transactions'}
          component={Transaction}
          tabbar
          options={{tabBarLabelStyle: {top: -10}}}
        />
        <Tab.Screen
          name={'Accounts'}
          component={Account}
          options={{tabBarLabelStyle: {top: -10}}}></Tab.Screen>
      </Tab.Navigator>
    </>
  );
}

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '# 7F5DF0',
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.5,
    elevation: 5,
  },
  textStyle: {
    fontSize: moderateScale(10),
  },
});
