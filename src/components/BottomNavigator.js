import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Colors from '../themes/Colors';

//import Wallet from '../screens/Wallet/Wallet';
import Report from '../screens/Report/Report';
import Transaction from '../screens/Transaction/Transaction';
import AddTransaction from '../screens/Transaction/AddTransaction';
import Account from '../screens/Account/Index';
// import Add from './screens/Add';
import AddWallet from '../screens/Wallet/AddWallet';
import {moderateScale} from '../libs/scaling';
import Wallet from '../screens/Wallet/Wallet';
import {PrimaryButton} from './Button';
import AddCategory from '../screens/Category/Index';

const Wallets = 'Wallet';
const addWallets = 'Add Wallet';

const AddCategories = 'Add Category';

const Reports = 'Report';

const Transactions = 'Transaction';
const AddTransactions = 'Add Transaction';

const Accounts = 'Account';

// const Adds = 'Add';
const Tab = createBottomTabNavigator();

const CustomTabBarButton = ({children, onPress}) => (
  <TouchableOpacity
    style={{
      top: -30,
      justifyContent: 'center',
      alignItems: 'center',
      ...styles.shadow,
    }}
    onPress={onPress}>
    <View
      style={{
        width: 70,
        height: 70,
        borderRadius: 35,
        backgroundColor: Colors.primary,
      }}>
      {children}
    </View>
  </TouchableOpacity>
);

export default function BottomNavigator(props) {
  return (
    <>
      <Tab.Navigator
        screenOptions={{
          headerTitleAlign: 'center',
          tabBarShowLabel: false,
          tabBarStyle: {
            position: 'absolute',
            bottom: 25,
            left: 20,
            right: 20,
            elevation: 0,
            backgroundColor: '#fff',
            borderRadius: 15,
            height: 90,
            ...styles.shadow,
          },
        }}>
        <Tab.Screen
          name={Wallets}
          component={Wallet}
          options={{
            tabBarIcon: ({focused}) => (
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  top: 10,
                }}>
                <Image
                  source={require('../assets/images/wallet-icon.png')}
                  resizeMode="contain"
                  style={{
                    width: 25,
                    height: 25,
                    tintColor: focused ? Colors.primary : '#748c94',
                  }}
                />
                <Text
                  style={{
                    color: focused ? Colors.primary : '#748c94',
                    fontSize: moderateScale(10),
                    fontWeight: 'bold',
                  }}>
                  WALLET
                </Text>
              </View>
            ),
          }}></Tab.Screen>
        <Tab.Screen
          name={AddCategories}
          component={AddCategory}
          options={{
            tabBarIcon: ({focused}) => (
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  top: 10,
                }}>
                <Image
                  source={require('../assets/images/report-icon.png')}
                  resizeMode="contain"
                  style={{
                    width: 25,
                    height: 25,
                    tintColor: focused ? Colors.primary : '#748c94',
                  }}
                />
                <Text
                  style={{
                    color: focused ? Colors.primary : '#748c94',
                    fontSize: moderateScale(10),
                    fontWeight: 'bold',
                  }}>
                  REPORT
                </Text>
              </View>
            ),
          }}></Tab.Screen>
        <Tab.Screen
          name={AddTransactions}
          component={AddTransaction}
          options={{
            headerShadowVisible: false,
            tabBarIcon: ({focused}) => (
              <View>
                <Image
                  source={require('../assets/images/plus-icon.png')}
                  resizeMode="contain"
                  style={{width: 30, height: 30, tintColor: '#fff'}}
                />
              </View>
            ),
            tabBarButton: props => <CustomTabBarButton {...props} />,
          }}></Tab.Screen>
        <Tab.Screen
          name={Transactions}
          component={Transaction}
          options={{
            tabBarIcon: ({focused}) => (
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  top: 10,
                }}>
                <Image
                  source={require('../assets/images/transaction-icon.png')}
                  resizeMode="contain"
                  style={{
                    width: 25,
                    height: 25,
                    tintColor: focused ? Colors.primary : '#748c94',
                  }}
                />
                <Text
                  style={{
                    color: focused ? Colors.primary : '#748c94',
                    fontSize:
                      Platform.OS === 'android'
                        ? moderateScale(9)
                        : moderateScale(8),
                    fontWeight: 'bold',
                  }}>
                  TRANSACTION
                </Text>
              </View>
            ),
          }}></Tab.Screen>
        <Tab.Screen
          name={Accounts}
          component={Account}
          options={{
            tabBarIcon: ({focused}) => (
              <View
                style={{
                  alignItems: 'center',
                  justifyContent: 'center',
                  top: 10,
                }}>
                <Image
                  source={require('../assets/images/account-icon.png')}
                  resizeMode="contain"
                  style={{
                    width: 25,
                    height: 25,
                    tintColor: focused ? Colors.primary : '#748c94',
                  }}
                />
                <Text
                  style={{
                    color: focused ? Colors.primary : '#748c94',
                    fontSize: moderateScale(10),
                    fontWeight: 'bold',
                  }}>
                  ACCOUNT
                </Text>
              </View>
            ),
          }}></Tab.Screen>
      </Tab.Navigator>
    </>
  );
}

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '# 7F5DF0',
    shadowOffset: {
      width: 0,
      height: 10,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.5,
    elevation: 5,
  },
  textStyle: {
    fontSize: moderateScale(10),
  },
});
