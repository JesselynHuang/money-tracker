import React, {useState, useEffect} from 'react';
import {View, Image, StyleSheet} from 'react-native';
import {moderateScale} from '../libs/scaling';
//import Badge from './Badge';
import Colors from '../themes/Colors';

const styles = StyleSheet.create({
  container: {
    width: moderateScale(24),
    height: moderateScale(24),
    paddingTop: moderateScale(6),
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default function IconWithBadge(props) {
  const {name, focused} = props;

  const [image, setImage] = useState(null);

  useEffect(() => {
    let imageSrc;
    switch (props.name) {
      case 'Wallets':
        imageSrc = require('./../assets/images/wallet-icon.png');
        break;

      case 'Reports':
        imageSrc = require('./../assets/images/planning-icon.png');
        break;

      case 'Add Transaction':
        imageSrc = require('./../assets/images/plus-icon.png');
        break;

      case 'Transactions':
        imageSrc = require('./../assets/images/transaction-icon.png');
        break;

      case 'Accounts':
        imageSrc = require('./../assets/images/account-icon.png');
        break;

      default:
        break;
    }
    setImage(imageSrc);
  }, [name]);

  return (
    <View style={styles.container}>
      <Image
        style={{
          width: moderateScale(24),
          height: moderateScale(24),
          tintColor: focused ? Colors.primary : Colors.black,
        }}
        source={image}
      />
      {/* <Badge count={badgeCount} /> */}
    </View>
  );
}
