import React, {useState, useEffect} from 'react';
import {View} from 'react-native';
import {moderateScale} from '../libs/scaling';
import helper from '../libs/helper';
import FastImage from 'react-native-fast-image';

const DEFAULT_IMAGE = require('../assets/images/profile_placeholder-100.png');

export default function Avatar(props) {
  const {source, size = moderateScale(42), children, style} = props;

  const [dataSource, setDataSource] = useState(DEFAULT_IMAGE);

  useEffect(() => {
    const _loadToken = async () => {
      const dataToken = await Helper.getToken();
      if (source) {
        setDataSource({
          uri: source,
          headers: {
            Authorization: `Bearer ${dataToken}`,
          },
        });
      }
    };
    _loadToken();
  }, [source]);

  return (
    <View
      style={[
        {
          width: size,
          height: size,
        },
        style,
      ]}>
      <FastImage
        style={{
          width: size,
          height: size,
          borderRadius: size * 0.5,
        }}
        source={dataSource}
        onError={() => setDataSource(DEFAULT_IMAGE)}
      />
      {children}
    </View>
  );
}
