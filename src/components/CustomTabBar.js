import React from 'react';
import {View, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {CardView} from './CardPress';
import {Text} from './Text';
import {moderateScale} from '../libs/scaling';
import {BTN_CENTER} from '../constants';
import {connect} from 'react-redux';
import {Colors} from '../themes';

const defaultImage = require('../assets/images/group-colored-24.png');
