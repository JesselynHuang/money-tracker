import React, {useEffect, useRef} from 'react';
import {FlatList as DefaultFlatList} from 'react-native';

export default function FlatList(props) {
  const {children, style, onLimitUp, onLimitDown, flatListRef, ...restProps} =
    props;

  const REF_FLATLIST = useRef();

  useEffect(() => {
    if (flatListRef) {
      flatListRef(REF_FLATLIST.current);
    }
  }, []);

  const _handleScroll = event => {
    try {
      const limit = event.nativeEvent.contentOffset.y;
      if (limit > 30) {
        if (onLimitUp) {
          onLimitUp();
        }
      } else {
        if (onLimitDown) {
          onLimitDown();
        }
      }
    } catch (error) {
      console.log('_handleScroll => ', error);
    }
  };

  return (
    <DefaultFlatList
      style={[
        {
          flex: 1,
        },
        style,
      ]}
      ref={REF_FLATLIST}
      keyExtractor={(_, index) => index.toString()}
      onScroll={_handleScroll}
      scrollEventThrottle={500}
      showsHorizontalScrollIndicator={false}
      showsVerticalScrollIndicator={false}
      {...restProps}>
      {children}
    </DefaultFlatList>
  );
}
