const Colors = {
  primary: '#e32f45',
  primaryDark: '#0E5197',
  accent: '#ffbd01',
  black: '#000',
  white: '#fff',
  greyButton: 'rgba(23, 27, 46, 0.08)',
  textDark: '#212121',
  lightText: 'rgba(23, 27, 46, 0.6)',
  paleText: 'rgba(23, 27, 46, 0.8)',
  divider: 'rgba(23, 27, 46, 0.2)',
  bgContainer: 'rgba(23, 27, 46, 0.1)',
  greyishBrown: '#444444',
  green: '#56CF6A',
  blue: '#3AF5CF',
  grayShadow: '#BBBBBB',
  red: 'red',
  darkBlue: '#0f5198',
  greyBackground: '#d0d6de',
  bgBackground: '#ededed',
};

export default Colors;
