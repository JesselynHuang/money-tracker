import moment from 'moment';
export const Helper = {
  nowDate: () => {
    return moment().format('YYYY-MM-DD').toString();
  },
  getDateTimeNow: () => {
    return moment().toString();
  },
  nowTimeFull: () => {
    return moment().toString('hh:mm:ss');
  },
  dateFormat: date => {
    return moment(date).format('YYYY-MM-DD');
  },
  timeFormat12H: date => {
    return moment(date).format('hh:mm A');
  },
  timeFormat24H: date => {
    return moment(date).format('HH:mm');
  },
  dateHalfMonth: date => {
    return moment(date).format('DD MMM YYYY');
  },
  dateFullMonth: date => {
    return moment(date).format('DD MMMM YYYY');
  },
  getDayNameNow: () => {
    return moment().format('dddd').toLowerCase();
  },
};
