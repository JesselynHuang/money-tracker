import {Dimensions, PixelRatio, Platform} from 'react-native';

const {width, height} = Dimensions.get('window');
const [shortDimension, longDimension] =
  width < height ? [width, height] : [height, width];

//Default guideline sizes are based on standard ~5" screen mobile device
const guidelineBaseWidth = 350;
const guidelineBaseHeight = 680;

const scale = size => (shortDimension / guidelineBaseWidth) * size;
const verticalScale = size => (longDimension / guidelineBaseHeight) * size;
const moderateScale = (size, factor = 0.5) => {
  if (Platform.OS === 'ios') {
    return size + (scale(size) - size) * factor;
  } else {
    const newSize = size * (width / 350);
    return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 1;
  }
};

export {scale, verticalScale, moderateScale};
