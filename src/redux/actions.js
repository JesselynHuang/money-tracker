export const SET_CATEGORY = 'SET_CATEGORY';

export const ADD_WALLET = 'ADD_WALLET';

export const ADD_TRANSACTION = 'ADD_TRANSACTION';

export const ADD_CATEGORIES = 'ADD_CATEGORIES';

export const setCategory = category => dispatch => {
  dispatch({
    type: SET_CATEGORY,
    payload: category,
  });
};

export const setWallet = args => {
  return {
    type: ADD_WALLET,
    args,
  };
};

export const addTransaction = args => {
  return {
    type: ADD_TRANSACTION,
    args,
  };
};

export const setCategories = args => {
  return {
    type: ADD_CATEGORIES,
    args,
  };
};
