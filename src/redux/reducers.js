'use strict';
import {Record} from 'immutable';
import {
  ADD_CATEGORIES,
  ADD_TRANSACTION,
  ADD_WALLET,
  SET_CATEGORY,
} from './actions';

const objectRecord = new Record({
  wallets: [],
  category: '',
  transactions: [],
  categories: [],
});

const initialState = new objectRecord();

const wallet = (state = initialState, action) => {
  switch (action.type) {
    // case ADD_WALLET:
    //   console.log('payload wallet', action.payload);
    //   //console.log('state wallet', ...state.wallets);
    //   return {
    //     wallets: [...state.wallets, action.payload],
    //   };
    case ADD_WALLET:
      let dataWallet = [...state.get('wallets')];
      dataWallet.push(action.args);
      return state.set('wallets', dataWallet);

    case SET_CATEGORY:
      return {
        ...state,
        category: action.payload,
      };

    case ADD_TRANSACTION:
      let dataTransaction = [...state.get('transactions')];
      dataTransaction.push(action.args);
      return state.set('transactions', dataTransaction);

    case ADD_CATEGORIES:
      // console.log('payload categori', action.payload);
      let dataCategories = [...state.get('categories')];
      dataCategories.push(action.args);
      return state.set('categories', dataCategories);

    default:
      return state;
  }
};

export default wallet;
