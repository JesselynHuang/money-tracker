import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import wallet from './reducers';

const rootReducer = combineReducers({wallet});

export const Store = createStore(rootReducer, applyMiddleware(thunk));
