import React, {useRef, useEffect} from 'react';
import {TouchableOpacity, Image} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import {moderateScale} from './libs/scaling';
import Colors from './themes/Colors';

import AddWallet from './screens/Wallet/AddWallet';
import AddTransaction from './screens/Transaction/AddTransaction';
import Dashboard from './components/Dashboard';
import Category from './screens/Category/Index';
import AddNewCategory from './screens/Category/AddNewCategory';
import AddIcon from './screens/Icon/Index';
const Stack = createStackNavigator();

const defaultStyle = {
  headerStyle: {
    borderBottomWidth: 0, //for ios
    shadowOpacity: 0, //for ios
    elevation: 0, //for android,
    backgroundColor: Colors.white,
  },
  headerTitleStyle: {
    color: Colors.textDark,
  },
  shadowColor: 'transparent',
  borderBottomWidth: 0,
  headerBackTitle: ' ',
  headerTitleAlign: 'center',
};

export default function StackNavigation(props) {
  const REF_NAV = useRef();

  useEffect(() => {
    if (REF_NAV && REF_NAV.current) {
      props.stackRef(REF_NAV);
    }
  }, [REF_NAV]);

  return (
    <NavigationContainer ref={REF_NAV}>
      <Stack.Navigator
        initialRouteName={'Wallet'}
        screenOptions={({navigation}) => ({
          ...defaultStyle,
          headerBackImage: () => {
            return (
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{padding: moderateScale(8)}}>
                <Image
                  source={require('./assets/images/arrow_left.png')}
                  style={{tintColor: Colors.white}}
                />
              </TouchableOpacity>
            );
          },
        })}>
        <Stack.Screen
          name={'Money Tracker'}
          component={Dashboard}
          options={{headerShown: false}}
        />

        <Stack.Screen name={'Add Wallet'} component={AddWallet} />

        <Stack.Screen name={'Choose Category'} component={Category} />
        <Stack.Screen name={'Add New Category'} component={AddNewCategory} />
        <Stack.Screen name={'Add Transaction'} component={AddTransaction} />
        <Stack.Screen name={'Add Icon'} component={AddIcon} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
